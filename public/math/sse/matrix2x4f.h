#ifndef HGUARD_LIBEX_MATH_SSE_MATRIX2x4f
#define HGUARD_LIBEX_MATH_SSE_MATRIX2x4f
#pragma once

#include "defs.h"
#include "../matrix.h"

namespace math
{

template<>
class MATH_ALIGN16 matrix<float,2,4> {
public:
	typedef float type;
	static const dim ROWS = 2;
	static const dim COLUMNS = 4;

	typedef matrix<float,2,4> value;
	typedef matrix<float,2,4>& ref;
	typedef const matrix<float,2,4>& const_ref;

	//----------------------------------------------------------------
	// Construction
	//----------------------------------------------------------------

	matrix();
	matrix( type f );
	matrix( type a00, type a01, type a02, type a03, type a10, type a11, type a12, type a13 );
	matrix( const type* mat );
	matrix( __m128 row0, __m128 row1 );

	//----------------------------------------------------------------
	// Assignment
	//----------------------------------------------------------------

	static value zero();
	static value identity();
	static value translate( type dx, type dy );
	static value scale( type s );
	static value scale( type sx, type sy );

	vector4f operator[] ( dim i ) const;

	type* data();
	const type* data() const;

	type& operator() ( dim row, dim col );
	const type& operator() ( dim row, dim col ) const;

private:
	__m128 regs[2];
};

typedef matrix<float,2,4> matrix2x4f;

}

#endif // !HGUARD_LIBEX_MATH_SSE_MATRIX2x4f
