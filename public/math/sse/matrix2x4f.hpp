#ifndef HGUARD_LIBEX_MATH_SSE_MATRIX2x4f_HPP
#define HGUARD_LIBEX_MATH_SSE_MATRIX2x4f_HPP
#pragma once

#include "matrix2x4f.h"

namespace math
{

inline matrix2x4f::matrix() {
}
inline matrix2x4f::matrix( type f ) {
	regs[0] = _mm_set1_ps( f );
	regs[1] = _mm_set1_ps( f );
}
inline matrix2x4f::matrix( type a00, type a01, type a02, type a03, type a10, type a11, type a12, type a13 ) {
	regs[0] = _mm_set_ps( a03, a02, a01, a00 );
	regs[1] = _mm_set_ps( a13, a12, a11, a10 );
}
inline matrix2x4f::matrix( const type* mat ) {
	regs[0] = _mm_loadu_ps( mat+0 );
	regs[1] = _mm_loadu_ps( mat+4 );
}
inline matrix2x4f::matrix( __m128 row0, __m128 row1 ) {
	regs[0] = row0;
	regs[1] = row1;
}
inline matrix2x4f matrix2x4f::zero() {
	return matrix2x4f(0.f);
}
inline matrix2x4f matrix2x4f::identity() {
	return matrix2x4f(1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f);
}
inline matrix2x4f matrix2x4f::translate( type dx, type dy ) {
	__m128 a = _mm_move_ss( _mm_set_ps(0.f,0.f,1.f,0.f), _mm_load_ps(&dx) );
	a = _mm_shuffle_ps( a, a, _MM_SHUFFLE(3,0,3,1) );
	__m128 b = _mm_move_ss( _mm_set_ps(0.f,0.f,1.f,0.f), _mm_load_ps(&dy) );
	b = _mm_shuffle_ps( b, b, _MM_SHUFFLE(3,0,1,3) );
	return matrix2x4f( a, b );
}
inline matrix2x4f matrix2x4f::scale( type s ) {
	__m128 a = _mm_move_ss( _mm_setzero_ps(), _mm_load_ps(&s) );
	__m128 b = _mm_shuffle_ps( a, a, _MM_SHUFFLE(3,2,1,0) );
	__m128 c = _mm_shuffle_ps( a, a, _MM_SHUFFLE(3,2,0,1) );
	return matrix2x4f( b, c );
}
inline matrix2x4f matrix2x4f::scale( type sx, type sy ) {
	__m128 a = _mm_move_ss( _mm_setzero_ps(), _mm_load_ps(&sx) );
	a = _mm_shuffle_ps( a, a, _MM_SHUFFLE(3,2,1,0) );
	__m128 b = _mm_move_ss( _mm_setzero_ps(), _mm_load_ps(&sy) );
	b = _mm_shuffle_ps( b, b, _MM_SHUFFLE(3,2,0,1) );
	return matrix2x4f( a, b );
}
inline float* matrix2x4f::data() {
	return regs[0].m128_f32;
}
inline const float* matrix2x4f::data() const {
	return regs[0].m128_f32;
}
inline vector4f matrix2x4f::operator[] ( dim i ) const {
	assert( i<2 );
	return vector4f( regs[i] );
}
inline float& matrix2x4f::operator() ( dim row, dim col ) {
	assert( row<2 && col<4 );
	return regs[row].m128_f32[col];
}
inline const float& matrix2x4f::operator() ( dim row, dim col ) const {
	assert( row<2 && col<4 );
	return regs[row].m128_f32[col];
}

}

#endif // !HGUARD_LIBEX_MATH_SSE_MATRIX2x4f_HPP
