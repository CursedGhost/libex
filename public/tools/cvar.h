#pragma once

//----------------------------------------------------------------
// Tools: Cvar system
//----------------------------------------------------------------
//
// Organizes cvars in a tree structure.
//
// Design rationale:
/*
Cvars (short for console variables) are designed as a simple
mechanism to allow interaction between a program and a human.
Humans expect a textual representation while the program's
native format is its raw binary interpretation (int, float, etc).

This is modelled with a shared interface that accepts and outputs
the textual representation while storing it in whatever format is
most convenient to the program. The cvar's job is thus to convert
the textual representation to its internal format and vica versa.

Because trees are awesome the cvars are stored as such.

There's also command nodes for extra interaction.
*/
//

#include <exception>
#include <string>
#include <vector>
#include <type_traits>

#include "enumstring.h"
#include "printf.h"
#include "../color/argb.h"
#include "../hash/idtype.h"

/// Defines the cvar descriptions as hashed identifiers supposed to be used with a localization system.
/// All it does is store descriptions as (unsigned) integers, it does not come with a localization system (batteries not included).
#define CVAR_LOCALIZE

/// Calling convention used for all function pointers.
/// Mostly only used 
#define CVAR_CALL __fastcall

#define CVAR_NOVTABLE __declspec(novtable)

namespace tools
{

//------------------------------------------------
// Declarations
//------------------------------------------------

class CEnumBase;
template< typename Char > class CCmdArgs;

class cvar_node;
class cvar_tree;
class cvar_collect;


/// A global identifier. When a module is unloaded this allows automatic cleanup of all its cvars.
typedef uintptr_t cvar_dllid_t;
#ifdef CVAR_LOCALIZE
/// Cvar description type.
/// Stores the description has a hashed identifier meant to be used with an external localization system, batteries not included.
typedef hash::IdType<char> cvar_desc_t;
#else
/// Cvar description type.
typedef const char* cvar_desc_t;
#endif // CVAR_LOCALIZE
/// Native string type used by the cvar library, interface must be compatible with std's string class.
typedef std::string cvar_string_t;
/// Vector storing autocomplete data, interface must be compatible with std's vector class.
typedef std::vector<cvar_string_t> cvar_completion_t;
/// Native color type.
typedef color::argb cvar_color_t;
/// Arguments processing.
typedef CCmdArgs<char> cvar_arguments;


//------------------------------------------------
// Helpers
//------------------------------------------------

/// Fill the auto completion list as requested. Pushes the item on the list if partial has a substring item (case-sensitive).
/// \param item Item to be added to the autocomplete list.
/// \param partial User input so far, may be nullptr or empty string (will always pass).
/// \param list Autocomplete list generated so far.
/// \return True if the item was added.
bool cvar_partial( const char* item, const char* partial, cvar_completion_t& list );

/// All cvar errors, not ideal but for now it'll do...
class cvar_error : public std::exception
{
public:
	cvar_error( const char* desc ) : std::exception(desc) { }
};


//------------------------------------------------
// Cvar interface
//------------------------------------------------

// Cvar node types
// FIXME! Throw all this away and get a proper dynamic cast type of thing?
enum cvartype_t
{
	CVAR_UNKNOWN,
	CVAR_SUBTREE,
	CVAR_VALUE,
	CVAR_COMMAND,
};
	
/// Flags for cvars.
enum
{
	/// No special flags.
	FLCVAR_VOID = 0,
	/// The cvar will not be registered in Release builds.
	FLCVAR_DEVONLY = (1<<0),
	/// The cvar will be registered but won't be displayed in cvar listings or autocomplete.
	FLCVAR_HIDDEN = (1<<1),
	/// Make a malloc copy of the cvar name. Use for cvars with a name generated at runtime.
	FLCVAR_NAMECOPY = (1<<2),
	/// Automatically manage the cvar by delete'ing it when the parent tree node is destructed. Use for cvars created with new, just register & forget.
	FLCVAR_MANAGED = (1<<3),
	/// Contains sensitive information, do not display. The application must check this flag itself, this library does not prevent get()'ing its value!
	FLCVAR_PASSWORD = (1<<4),
	/// This is a read-only cvar, throws an error when the user attempts to change it.
	FLCVAR_READONLY = (1<<5),
};
/// Define the cvar flags as an unsigned int to make them less painful to work with.
typedef unsigned int flcvar_t;

/// Shared interface for cvars.
class CVAR_NOVTABLE cvar_node
{
	/// Disallow copy constructor.
	cvar_node( const cvar_node& );
public:
	friend class cvar_collect;

	/// Constructor.
	/// \param name Name of this cvar. If the name is static for the lifetime of this cvar, add FLCVAR_NAMECOPY to malloc a copy.
	/// \param desc Description of this cvar. Must be always be static for the lifetime of this cvar!
	/// \param flags Flags of this cvar.
	cvar_node( const char* name, cvar_desc_t desc, flcvar_t flags );
	/// Virtual destructor.
	virtual ~cvar_node();

	// Get the actual node type.
	virtual int type() const = 0;
	/// Get the dll identifier for this node.
	virtual cvar_dllid_t dllid() const;
	/// Get the global identifier for this module.
	static cvar_dllid_t globalid();

	/// Get the full, dot separated, name of this cvar.
	cvar_string_t& fullname( cvar_string_t& name ) const;

	/// Get its name.
	inline const char* name() const { return _name; }
	/// Get its description.
	inline cvar_desc_t desc() const { return _desc; }
	/// Get its flags.
	inline flcvar_t flags() const { return _flags; }
	/// Get its parent.
	inline cvar_tree* parent() const { return _parent; }
	/// Get its sibling.
	inline cvar_node* next() const { return _next; }

protected:
	const char* _name;
	cvar_desc_t _desc;
	flcvar_t _flags;
	cvar_tree* _parent;
	cvar_node* _next;
};

/// Cvar value nodes.
class CVAR_NOVTABLE cvar_value : public cvar_node
{
public:
	/// Constructor. See base class for more info.
	cvar_value( const char* name, cvar_desc_t desc, flcvar_t flags );
		
	// Inherited from cvar_node
	virtual int type() const;

	/// Get the value for this cvar.
	/// \return The value of this cvar as a string.
	virtual cvar_string_t get() const = 0;

	/// Set a new value for this cvar.
	/// \param val The new value for this cvar, may throw on error.
	virtual void set( const char* val ) = 0;

	/// Reset the cvar to its default value.
	virtual void reset() = 0;

	/// Auto complete the possible values for this cvar.
	/// /param partial Input typed so far, only add values that contain this as a substring.
	/// /param list Autocomplete list generated so far.
	/// /return False if not applicable and no values were added.
	virtual bool values( const char* partial, cvar_completion_t& list ) const;
};

/// Cvar value nodes shared implementation.
template< typename Type, typename Traits = cvar_traits<Type> >
class cvar_var : public cvar_value
{
public:
	typedef typename Traits traits;

	/// Value type of this cvar.
	typedef typename traits::value_t value_t;
	/// Pass the underlying value as this type to function calls.
	typedef typename traits::param_t param_t;
	/// Return the underlying value as this type.
	typedef typename const traits::value_t& return_t;
	/// Return the underlying value as a pointer to this type
	typedef typename const traits::value_t* returnp_t;

	cvar_var( const char* name, cvar_desc_t desc, flcvar_t flags, param_t init );

	virtual cvar_string_t get() const;
	virtual void set( const char* val );
	virtual void reset();
	virtual bool values( const char* partial, cvar_completion_t& list ) const;

	/// Give the cvar a chance to edit the value after it was parsed and before it is stored.
	/// /param val New value to be assigned to this cvar.
	virtual void store( value_t& val );

	/// Set the internal value, does not pass onchange.
	void value( param_t val );
	/// Get the internal value.
	return_t value() const;

	/// Convenience assignment operator.
	inline void operator= ( param_t val ) { value(val); }
	/// Convenience type operator.
	inline operator return_t () const { return value(); }
	/// Convenience dereference operator.
	inline return_t operator* () const { return value(); }
	/// Convenience pointer operator.
	inline returnp_t operator-> () const { return &value(); }

protected:
	value_t _default;
	value_t _value;
};

/// Cvar value with a minimum bound.
template< typename Base >
class cvar_minbound : public Base
{
public:
	typedef typename Base::value_t value_t;
	typedef typename Base::param_t param_t;
	cvar_minbound( const char* name, cvar_desc_t desc, flcvar_t flags, param_t init, param_t minval );
	virtual void store( value_t& val );
	using Base::operator=;
protected:
	value_t _minval;
};

/// Cvar value with a maximum bound.
template< typename Base >
class cvar_maxbound : public Base
{
public:
	typedef typename Base::value_t value_t;
	typedef typename Base::param_t param_t;
	cvar_maxbound( const char* name, cvar_desc_t desc, flcvar_t flags, param_t init, param_t maxval );
	virtual void store( value_t& val );
	using Base::operator=;
protected:
	value_t _maxval;
};

/// Cvar value with min and max bounds.
template< typename Base >
class cvar_bounded : public Base
{
public:
	typedef typename Base::value_t value_t;
	typedef typename Base::param_t param_t;
	cvar_bounded( const char* name, cvar_desc_t desc, flcvar_t flags, param_t init, param_t minval, param_t maxval );
	virtual void store( value_t& val );
	using Base::operator=;
protected:
	value_t _minval;
	value_t _maxval;
};

/// Base class for enum nodes.
/// This class isn't templated, so only one copy of the code/vtable will be generated!
class cvar_enumbase : public cvar_value
{
public:
	typedef CEnumBase::enum_t enum_t;

	cvar_enumbase( const char* name, cvar_desc_t desc, flcvar_t flags, const CEnumBase& en, enum_t init );

	virtual cvar_string_t get() const;
	virtual void set( const char* val );
	virtual void reset();
	virtual bool values( const char* partial, cvar_completion_t& list ) const;

	/// Give the cvar a chance to edit the value after it was parsed and before it is stored.
	/// /param val New value to be assigned to this cvar.
	virtual void store( enum_t& val );

	/// Set the internal value, does not pass onchange.
	void value( enum_t e );
	/// Get the internal value.
	enum_t value() const;

	/// Get the enum instance.
	const CEnumBase& inst() const;

protected:
	const CEnumBase& _enum;
	enum_t _default;
	enum_t _value;
};

/// Enum value nodes.
/// Just a convenience wrapper, no extra (no-inline) code is written here to avoid a new class instance for every enum.
/// CVAR_NOVTABLE is really important otherwise the compiler will generate a vtable & type information for every enum.
template< typename Enum >
class CVAR_NOVTABLE cvar_enum : public cvar_enumbase
{
private:
	cvar_enumbase::value;
public:
	cvar_enum( const char* name, cvar_desc_t desc, flcvar_t flags, Enum init );

	/// Set the internal value, does not pass onchange.
	void value( Enum e );
	/// Get the internal value.
	Enum value() const;

	/// Convenience assignment operator.
	inline void operator= ( Enum val ) { value(val); }
	/// Convenience type operator.
	inline operator Enum () const { return value(); }
};


/// Traits specify details about how to encode/decode the value for this type.
template< typename Type >
struct cvar_traits;

template<>
struct cvar_traits<int>
{
	typedef int value_t;
	typedef int param_t;
	typedef cvar_var<int> cvar_t;
	static void parse( cvar_t& cvar, const char* begin, const char* end, int& val );
	static cvar_string_t format( const cvar_t& cvar, int val );
};
template<>
struct cvar_traits<float>
{
	typedef float value_t;
	typedef const float& param_t;
	typedef cvar_var<float> cvar_t;
	static void parse( cvar_t& cvar, const char* begin, const char* end, float& val );
	static cvar_string_t format( const cvar_t& cvar, const float& val );
};
template<>
struct cvar_traits<cvar_string_t>
{
	typedef cvar_string_t value_t;
	typedef const char* param_t;
	typedef cvar_var<cvar_string_t> cvar_t;
	static void parse( cvar_t& cvar, const char* begin, const char* end, cvar_string_t& val );
	static cvar_string_t format( const cvar_t& cvar, const cvar_string_t& val );
};
template<>
struct cvar_traits<cvar_color_t>
{
	typedef cvar_color_t value_t;
	typedef cvar_color_t param_t;
	typedef cvar_var<cvar_color_t> cvar_t;
	static void parse( cvar_t& cvar, const char* begin, const char* end, cvar_color_t& val );
	static cvar_string_t format( const cvar_t& cvar, cvar_color_t val );
};

/// Integer value nodes.
typedef cvar_var<int> cvar_int;
/// Float value nodes.
typedef cvar_var<float> cvar_float;
/// String value nodes.
typedef cvar_var<cvar_string_t> cvar_string;
/// Color value nodes.
typedef cvar_var<cvar_color_t> cvar_color;
/// Boolean value nodes.
typedef cvar_enum<bool> cvar_bool;



//------------------------------------------------
// Cvar tree nodes
//------------------------------------------------

/// Interface for tree nodes.
class CVAR_NOVTABLE cvar_tree : public cvar_node
{
public:
	/// Constructor. See base class for more info.
	cvar_tree( const char* name, cvar_desc_t desc, flcvar_t flags );

	virtual int type() const;

	/// Inserts a node.
	/// \param node The node to be inserted.
	virtual void insert( cvar_node* node ) = 0;

	/// Removes a node. Does not delete the node if is has FLCVAR_MANAGED!
	/// \param node The node to be removed, must be a direct child!
	/// \return Returns the removed node.
	virtual cvar_node* remove( cvar_node* node ) = 0;

	/// Find a child by name recursively.
	/// \param name The name of the node to find.
	/// \return Returns nullptr if not found.
	virtual cvar_node* find( const char* name ) const = 0;

	/// Get the first child.
	/// \return Returns the first child, nullptr if there are no children.
	virtual cvar_node* nodes() const = 0;

	/// Erase all nodes with dll identifier recursively.
	/// \param id All nodes with matching dll identifiers are removed (and deleted if FLCVAR_MANAGED).
	virtual void erase( cvar_dllid_t id ) = 0;

	/// Get a list of the child nodes related to partial.
	/// \param partial User input so far, may be nullptr or empty string (will always pass).
	/// \param list Autocomplete list generated so far.
	/// \param prefix The list stores only local names, prefix + list[i] = full name.
	/// \return Always true.
	virtual bool names( const char* partial, cvar_completion_t& list, cvar_string_t& prefix ) const = 0;

	/// Prepends prefix to the list items for you.
			bool names( const char* partial, cvar_completion_t& list ) const;
};

/// Implements cvar_tree.
class cvar_collect : public cvar_tree
{
public:
	/// Constructor. See base class for more info.
	cvar_collect( const char* name, cvar_desc_t desc, flcvar_t flags );

	virtual ~cvar_collect();
	virtual void insert( cvar_node* node );
	virtual cvar_node* remove( cvar_node* node );
	virtual cvar_node* find( const char* name ) const;
	virtual cvar_node* nodes() const;
	virtual void erase( cvar_dllid_t id );
	virtual bool names( const char* partial, cvar_completion_t& list, cvar_string_t& prefix ) const;
	using cvar_tree::names;
	
protected:
	// Finds a node locally
	cvar_node* find_local( const char*& name ) const;
	static bool find_cmp( const char* node, const char* begin, const char* end );

protected:
	cvar_node* _list;
};



//------------------------------------------------
// Cvar command nodes
//------------------------------------------------

/// Interface for command nodes.
class CVAR_NOVTABLE cvar_command : public cvar_node
{
public:
	/// Constructor. See base class for more info.
	cvar_command( const char* name, cvar_desc_t desc, flcvar_t flags );

	virtual int type() const;

	/// Invoke the callback.
	/// \param args Arguments passed to the callback.
	virtual void invoke( const cvar_arguments& args ) const = 0;

	/// Custom autocomplete.
	/// \param partial User input so far, may be nullptr or empty string (will always pass).
	/// \param list Autocomplete list generated so far.
	/// \return False if not applicable.
	virtual bool complete( const char* partial, cvar_completion_t& list ) const = 0;

	/// Invoke callback convenience.
	/// \param args Arguments passed to the callback.
	void operator() ( const cvar_arguments& args ) const;
};

/// Command implementation for member function callbacks.
class cvar_delegate : public cvar_command
{
public:
	typedef void (CVAR_CALL* Fn)( void* thisptr, const cvar_command* cvar, const cvar_arguments& args );
	typedef bool (CVAR_CALL* Cb)( void* thisptr, const cvar_command* cvar, const char* partial, cvar_completion_t& list );
	
	/// Constructor.
	/// \param name Name of this cvar. If the name is static for the lifetime of this cvar, add FLCVAR_NAMECOPY to malloc a copy.
	/// \param desc Description of this cvar. Must be always be static for the lifetime of this cvar!
	/// \param flags Flags of this cvar.
	/// \param ctx This pointer to use in the callback.
	/// \param fn Callback called on invoke(), use cvar_thunk wrapper below. Optional.
	/// \param cb Callback called on complete() to provide custom auto complete, use cvar_thunk wrapper below. Optional.
	cvar_delegate( const char* name, cvar_desc_t desc, flcvar_t flags, void* ctx, Fn fn = nullptr, Cb cb = nullptr );
	
	virtual void invoke( const cvar_arguments& args ) const;
	virtual bool complete( const char* partial, cvar_completion_t& list ) const;

protected:
	void* _user;
	Fn _func;
	Cb _complete;
};

/// Automatic dispatch to member function, to be used in the delegate's command callback.
/// \code
/// &cvar_thunk<CMyClass,&CMyClass::Command>
/// \encode
/// \tparam T Class of the member function.
/// \tparam Fn Member function to invoke as command callback.
template< typename T, void (T::*Fn)( const cvar_arguments& ) >
void CVAR_CALL cvar_thunk( void* thisptr, const cvar_command* cvar, const cvar_arguments& args )
{
	(((T*)thisptr)->*Fn)( args );
}

/// Automatic dispatch to member function, to be used in the delegate's completion callback.
/// \code
/// &cvar_thunk<CMyClass,&CMyClass::AutoComplete>
/// \encode
/// \tparam T Class of the member function.
/// \tparam Fn Member function to invoke as completion callback.
template< typename T, bool (T::*Fn)( const char*, cvar_completion_t& ) >
bool CVAR_CALL cvar_thunk( void* thisptr, const cvar_command* cvar, const char* partial, cvar_completion_t& list )
{
	return (((T*)thisptr)->*Fn)( partial, list );
}
	
}
