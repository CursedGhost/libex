
#include "base64.h"

namespace tools
{

void base64_encode( const unsigned char* it, unsigned int size, char* p )
{
	static const char basis[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	const unsigned char* end = it + size;
	for (; it < (end - 2); it += 3, p += 4) {
		unsigned char i1 = it[0];
		unsigned char i2 = it[1];
		p[0] = basis[i1 >> 2];
		p[1] = basis[((i1 & 0x3) << 4) | (i2 >> 4)];
		unsigned char i3 = it[2];
		p[2] = basis[((i2 & 0xF) << 2) | (i3 >> 6)];
		p[3] = basis[(i3 & 0x3F)];
	}

	size_t rem = end - it;
	if (rem == 1) {
		unsigned char i1 = it[0];
		unsigned char i2 = 0;
		p[0] = basis[i1 >> 2];
		p[1] = basis[((i1 & 0x3) << 4) | (i2 >> 4)];
		p[2] = '=';
		p[3] = '=';
		p += 4;
	}
	else if (rem == 2) {
		unsigned char i1 = it[0];
		unsigned char i2 = it[1];
		unsigned char i3 = 0;
		p[0] = basis[i1 >> 2];
		p[1] = basis[((i1 & 0x3) << 4) | (i2 >> 4)];
		p[2] = basis[((i2 & 0xF) << 2) | (i3 >> 6)];
		p[3] = '=';
		p += 4;
	}

	p[0] = 0;
}

bool base64_decode( const char* it, const char* end, unsigned char* p )
{
	static const unsigned char table[] = {
		62, -1, -1, -1, 63,
		52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, 0, -1, -1,
		-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
		15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
		-1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
		41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51
	};

	if ((end - it) % 4) {
		return false;
	}

	for (; it < end; it += 4, p += 3) {
		unsigned char c1 = it[0] - '+'; if (c1 >= sizeof(table)) goto fail;
		c1 = table[c1]; if (c1 == -1) goto fail;
		unsigned char c2 = it[1] - '+'; if (c1 >= sizeof(table)) goto fail;
		c2 = table[c2]; if (c2 == -1) goto fail;
		p[0] = (c1 << 2) | (c2 >> 4);
		unsigned char c3 = it[2] - '+'; if (c1 >= sizeof(table)) goto fail;
		c3 = table[c3]; if (c3 == -1) goto fail;
		p[1] = (c2 << 4) | (c3 >> 2);
		unsigned char c4 = it[3] - '+'; if (c1 >= sizeof(table)) goto fail;
		c4 = table[c4]; if (c4 == -1) goto fail;
		p[2] = (c3 << 6) | c4;
	}

	return true;
fail:
	return false;
}

}
