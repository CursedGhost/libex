
#include <cstdlib>
#include <cstdarg>

#include "cvar.hpp"

namespace tools
{

//------------------------------------------------
// Helpers
//------------------------------------------------

const char* cvar_parse_name(const char* cvar, char* name, unsigned size)
{
	unsigned s;
	// Find the next dot
	const char* it = cvar;
	for (; *it != '.'; ++it)
	{
		if (!*it)
		{
			s = static_cast<unsigned>(it - cvar);
			it = nullptr;
			goto done;
		}
	}
	s = static_cast<unsigned>(it - cvar);
	++it;
done:
	// Assign it to the given buffer
	s = (s >= size) ? (size - 1) : s;
	__movsb((unsigned char*)name, (const unsigned char*)cvar, s);
	name[s] = 0;
	// Return the subnodes
	return it;
}
template< unsigned int S > const char* cvar_parse_name(const char* cvar, char(&name)[S]) { return cvar_parse_name(cvar, name, S); }
bool cvar_partial( const char* str, const char* partial, cvar_completion_t& list )
{
	// FIXME! Case insensitive?
	if ( !partial || strstr( str, partial ) )
	{
		list.push_back( str );
		return true;
	}
	return false;
}



//------------------------------------------------
// Cvar node
//------------------------------------------------

cvar_node::cvar_node( const char* name, cvar_desc_t desc, unsigned flags )
	: _name(name), _desc(desc), _flags(flags), _parent(nullptr), _next(nullptr)
{
	// Keep a copy of the name.
	if ( flags&FLCVAR_NAMECOPY )
	{
		_name = strdup(name);
	}
}
cvar_node::~cvar_node()
{
	// If we still have a parent, remove us first.
	if ( _parent )
	{
		_parent->remove( this );
	}
	// Free memory if needed.
	if ( _flags&FLCVAR_NAMECOPY )
	{
		free( const_cast<char*>(_name) );
	}
}
cvar_dllid_t cvar_node::dllid() const
{
	// Use our module base as the identifier.
	return globalid();
}
cvar_dllid_t cvar_node::globalid()
{
	// HACK! To ensure a unique id for each module, just use the address of a static variable.
	static const cvar_dllid_t gid = (cvar_dllid_t) &gid;
	return gid;
}
cvar_string_t& cvar_node::fullname( cvar_string_t& s ) const
{
	// Exclude the root name (unless you ask the fullname of the root node).
	if ( _parent && _parent->parent() )
	{
		_parent->fullname( s );
		s.push_back( '.' );
	}
	s.append( _name );
	return s;
}


//------------------------------------------------
// Cvar value nodes
//------------------------------------------------


cvar_value::cvar_value( const char* name, cvar_desc_t desc, flcvar_t flags )
	: cvar_node(name, desc, flags) {}
int cvar_value::type() const
{
	return CVAR_VALUE;
}
void cvar_value::set( const char* s )
{
	if ( _flags&FLCVAR_READONLY )
	{
		cvar_string_t name;
		throw cvar_error( va_printf<64>( "Cannot assign \"%s\" to '%s' because it is read-only!", s, fullname(name).c_str() ) );
	}
}
bool cvar_value::values( const char* partial, cvar_completion_t& list ) const
{
	return false;
}

void cvar_traits<int>::parse( cvar_t& cvar, const char* begin, const char* end, int& val )
{
	val = atoi( begin );
}
cvar_string_t cvar_traits<int>::format( const cvar_t& cvar, int val )
{
	return cvar_string_t( va_printf<32,char>( "%d", val ) );
}
void cvar_traits<float>::parse( cvar_t& cvar, const char* begin, const char* end, float& val )
{
	val = static_cast<float>( atof( begin ) );
}
cvar_string_t cvar_traits<float>::format( const cvar_t& cvar, const float& val )
{
	return cvar_string_t( va_printf<32,char>( "%f", val ) );
}
void cvar_traits<cvar_string_t>::parse( cvar_t& cvar, const char* begin, const char* end, cvar_string_t& val )
{
	val.assign( begin, end );
}
cvar_string_t cvar_traits<cvar_string_t>::format( const cvar_t& cvar, const cvar_string_t& val )
{
	return val;
}
void cvar_traits<cvar_color_t>::parse( cvar_t& cvar, const char* begin, const char* end, cvar_color_t& val )
{
	// FIXME! Needs to pass the end of string explicitly!
	if ( !val.parse( begin ) )
	{
		throw cvar_error( "invalid color format!" );
	}
}
cvar_string_t cvar_traits<cvar_color_t>::format( const cvar_t& cvar, cvar_color_t val )
{
	cvar_string_t s;
	s.resize( 32 );
	s.resize( val.format( (char*)s.c_str() ) );
	return s;
}




cvar_enumbase::cvar_enumbase( const char* name, cvar_desc_t desc, flcvar_t flags, const CEnumBase& en, enum_t init )
	: cvar_value( name, desc, flags ), _enum(en), _default(init), _value(init) {}
cvar_string_t cvar_enumbase::get() const
{
	return cvar_string_t( _enum.Render<512>( _value ) );
}
void cvar_enumbase::set( const char* val )
{
	cvar_value::set( val );
	enum_t temp;
	_enum.Parse( val, temp, ES_THROW );
	store( temp );
}
void cvar_enumbase::reset()
{
	store( _default );
}
bool cvar_enumbase::values( const char* partial, cvar_completion_t& list ) const
{
	if ( !(flags()&FLCVAR_PASSWORD) )
	{
		// Add current value first
		cvar_partial( _enum.Render<512>(_value,ES_INT), partial, list );
		if ( _value!=_default )
		{
			// Followed by default value if it's different
			cvar_partial( _enum.Render<512>(_default,ES_INT), partial, list );
		}
		// Add the remaining values.
		enum_t e1, e2;
		const char* s;
		CEnumBase::temp_t temp;
		for ( int i = 0; s = _enum.Index( i, e1, temp ); ++i )
		{
			// Only show the first of duplicates as these are usually intended for convenience (but don't encourage this).
			if ( e1!=_value && e1!=_default && ( i==0 || e1!=e2 ) )
			{
				cvar_partial( s, partial, list );
			}
			e2 = e1;
		}
		return true;
	}
	return false;
}
void cvar_enumbase::store( enum_t& val )
{
	_value = val;
}


//------------------------------------------------
// Cvar tree nodes
//------------------------------------------------

int cvar_tree::type() const
{
	return CVAR_SUBTREE;
}
bool cvar_tree::names( const char* partial, cvar_completion_t& list ) const
{
	cvar_string_t prefix;
	bool b;
	if ( b = names( partial, list, prefix ) )
	{
		for ( auto it = list.begin(), end = list.end(); it!=end; ++it )
		{
			it->insert( 0, prefix );
		}
	}
	return b;
}


cvar_collect::~cvar_collect()
{
	// Orphan all our children
	while ( _list )
	{
		cvar_node* it = remove( _list );
		
		// If managed we need to clean it up
		if ( it->flags()&FLCVAR_MANAGED )
		{
			delete it;
		}
	}
}
void cvar_collect::insert( cvar_node* node )
{
	// Must not already have a parent!
	// NOTE! The code below should perfectly accept a chain of nodes without problem,
	//       however I want to reduce api complexity as much as possible (should lead to less bugs later on)!
	assert( node && !node->parent() && !node->next() );

#ifndef CVAR_ALLOWDEV
	// Not registering developer cvars
	if ( node->flags()&FLCVAR_DEVONLY ) return;
#endif // CVAR_ALLOWDEV

	// Make it our child
	node->_parent = this;

	// Add at the end
	// NOTE! Should a chain be added, extra care must be taken if added anywhere but the end (future bug-proofing :)
	if ( _list )
	{
		cvar_node* it;
		for ( it = _list; it->next(); it = it->next() );
		it->_next = node;
	}
	else
	{
		_list = node;
	}
}
cvar_node* cvar_collect::remove( cvar_node* node )
{
	// We can only remove our own children obviously
	assert( node && node->parent()==this );

	if ( node==_list )
	{
		_list = _list->_next;
done:
		node->_parent = nullptr;
		node->_next = nullptr;
		return node;
	}
	else
	{
		for ( cvar_node* it = _list; it; it = it->_next )
		{
			if ( it->_next==node )
			{
				it->_next = node->_next;
				goto done;
			}
		}
	}
	// What!? Should never happen!
	// We asserted ourselves as being its parent, but we didn't find it
	assert( false );
	return nullptr;
}
cvar_node* cvar_collect::find( const char* str ) const
{
	// Find the local cvar node referred to
	cvar_node* it = find_local( str );

	if ( it && str[0] )
	{
		// We need to go deeper, so this must be a subtree
		it = ( it->type()==CVAR_SUBTREE ) ? static_cast<cvar_tree*>( it )->find( str+1 ) : nullptr;
	}

	return it;
}
cvar_node* cvar_collect::nodes() const
{
	return _list;
}
void cvar_collect::erase( cvar_dllid_t id )
{
	cvar_node* next;
	for ( auto it = _list; it; it = next )
	{
		// Recurse as needed
		if ( it->type()==CVAR_SUBTREE )
		{
			static_cast<cvar_tree*>( it )->erase( id );
		}
		// Get next now, node might be deleted.
		next = it->next();
		// Remove if matching id
		if ( it->dllid()==id )
		{
			remove( it );
			// Cleanup memory if managed
			if ( it->flags()&FLCVAR_MANAGED )
			{
				delete it;
			}
		}
	}
}
bool cvar_collect::names( const char* partial, cvar_completion_t& list, cvar_string_t& prefix ) const
{
	const char* next = partial;
	const cvar_node* it = find_local( next );
	if ( next[0] )
	{
		// Not a valid subtree node, cannot autocomplete
		if ( !it || it->type()!=CVAR_SUBTREE )
		{
			return false;
		}
		// Append the prefix and recurse
		else
		{
			prefix.append( partial, next+1 );
			return static_cast<const cvar_tree*>( it )->names( next+1, list, prefix );
		}
	}

	// We want all the cvars starting with partial.
	for ( const cvar_node* it = _list; it; it = it->next() )
	{
		if (!( it->flags()&FLCVAR_HIDDEN ))
		{
			cvar_partial( it->name(), partial, list );
		}
	}
	return true;
}
cvar_node* cvar_collect::find_local( const char*& str ) const
{
	// Find the end
	static const char dot = '.';
	const char* end = str;
	for ( ; *end && *end!=dot; ++end ) {}

	// Return the end pointer
	const char* begin = str;
	str = end;

	// Find the node in the list
	for ( cvar_node* it = _list; it; it = it->next() )
	{
		if ( find_cmp( it->name(), begin, end ) )
		{
			return it;
		}
	}
	return nullptr;
}
bool cvar_collect::find_cmp( const char* str, const char* begin, const char* end )
{
	unsigned int i;
	for (i = 0; begin+i!=end && str[i]==begin[i]; ++i) {}
	return !str[i] && begin+i==end;
}

//------------------------------------------------
// Cvar command nodes
//------------------------------------------------

int cvar_command::type() const
{
	return CVAR_COMMAND;
}

void cvar_delegate::invoke( const cvar_arguments& args ) const
{
	return _func ? _func( _user, this, args ) : void();
}
bool cvar_delegate::complete( const char* partial, cvar_completion_t& list ) const
{
	return _complete ? _complete( _user, this, partial, list ) : false;
}

}
