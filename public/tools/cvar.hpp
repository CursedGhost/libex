#pragma once

#include "cvar.h"

namespace tools
{

template< typename Type, typename Traits >
cvar_var<Type,Traits>::cvar_var( const char* name, cvar_desc_t desc, flcvar_t flags, param_t init )
	: cvar_value(name, desc, flags), _default(init), _value(init) {}
template< typename T, typename Tr >
void cvar_var<T,Tr>::set( const char* val )
{
	// Throw error when read-only cvar.
	cvar_value::set( val );
	// Parse the new value (need a copy because parse may modify the existing value partially).
	value_t data = _value;
	traits::parse( *this, val, val+strlen(val), data );
	// Store the new value.
	store( data );
}
template< typename Type, typename Traits >
cvar_string_t cvar_var<Type,Traits>::get() const
{
	return traits::format( *this, _value );
}
template< typename Type, typename Traits >
void cvar_var<Type,Traits>::reset()
{
	store( _default );
}
template< typename Type, typename Traits >
void cvar_var<Type,Traits>::store( value_t& val )
{
	_value = val;
}
template< typename Type, typename Traits >
void cvar_var<Type,Traits>::value( param_t val )
{
	_value = val;
}
template< typename Type, typename Traits >
typename cvar_var<Type,Traits>::return_t cvar_var<Type,Traits>::value() const
{
	return _value;
}
template< typename Type, typename Traits >
bool cvar_var<Type,Traits>::values( const char* partial, cvar_completion_t& list ) const
{
	// Skip autocomplete for password cvars
	if ( !(flags()&FLCVAR_PASSWORD) )
	{
		// Add current value
		cvar_partial( traits::format( *this, _value ).c_str(), partial, list );
		// Add default value if it's different
		if ( !(_value==_default) )
		{
			cvar_partial( traits::format( *this, _default ).c_str(), partial, list );
		}
		return true;
	}
	return false;
}

template< typename T >
cvar_minbound<T>::cvar_minbound( const char* name, cvar_desc_t desc, flcvar_t flags, param_t init, param_t minval )
	: T(name, desc, flags, init), _minval(minval) {}
template< typename T >
void cvar_minbound<T>::store( value_t& val )
{
	_value = (val < _minval) ? _minval : val;
}

template< typename T >
cvar_maxbound<T>::cvar_maxbound( const char* name, cvar_desc_t desc, flcvar_t flags, param_t init, param_t maxval )
	: T(name, desc, flags, init), _maxval(maxval) {}
template< typename T >
void cvar_maxbound<T>::store( value_t& val )
{
	_value = (val > _maxval) ? _maxval : val;
}

template< typename T >
cvar_bounded<T>::cvar_bounded( const char* name, cvar_desc_t desc, flcvar_t flags, param_t init, param_t minval, param_t maxval )
	: T(name, desc, flags, init), _minval(minval), _maxval(maxval) {}
template< typename T >
void cvar_bounded<T>::store( value_t& val )
{
	_value = (val < _minval) ? _minval : (val > _maxval) ? _maxval : val;
}



inline void cvar_enumbase::value( enum_t e )
{
	_value = e;
}
inline cvar_enumbase::enum_t cvar_enumbase::value() const
{
	return _value;
}
inline const CEnumBase& cvar_enumbase::inst() const
{
	return _enum;
}
template< typename Enum >
cvar_enum<Enum>::cvar_enum( const char* name, cvar_desc_t desc, flcvar_t flags, Enum init )
	: cvar_enumbase( name, desc, flags, EnumStringFactory<Enum>(), init ) {}
template< typename Enum >
inline Enum cvar_enum<Enum>::value() const
{
	return enum_cast<Enum>( cvar_enumbase::value() );
}
template< typename Enum >
inline void cvar_enum<Enum>::value( Enum e )
{
	cvar_enumbase::value( static_cast<int>(e) );
}


inline cvar_tree::cvar_tree( const char* name, cvar_desc_t desc, flcvar_t flags )
	: cvar_node(name, desc, flags) {
}
inline cvar_collect::cvar_collect( const char* name, cvar_desc_t desc, flcvar_t flags )
	: cvar_tree(name, desc, flags), _list(nullptr) {
}


inline cvar_command::cvar_command( const char* name, cvar_desc_t desc, flcvar_t flags )
	: cvar_node(name, desc, flags) {
}
inline void cvar_command::operator() ( const cvar_arguments& args ) const
{
	invoke( args );
}
inline cvar_delegate::cvar_delegate( const char* name, cvar_desc_t desc, flcvar_t flags, void* ctx, Fn fn, Cb cb )
	: cvar_command(name, desc, flags), _user(ctx), _func(fn), _complete(cb) {
}

}
