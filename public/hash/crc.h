#ifndef HGUARD_LIBEX_HASH_CRC
#define HGUARD_LIBEX_HASH_CRC
#pragma once

#include "../libex.h"

namespace hash
{

typedef unsigned int crc32_t;

crc32_t crc32( const void* buf, size_t len, crc32_t crc = 0U );

template< typename T >
inline crc32_t crc32( const T& data, crc32_t crc = 0U )
{
	return crc32( &data, sizeof(data), crc );
}

}

#endif // !HGUARD_LIBEX_HASH_CRC
