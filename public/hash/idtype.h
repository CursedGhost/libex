#pragma once

#include "hash.h"

// Has to be forced inline or these might fail
#define inline INLINE
// For now, no constexpr support!
#define constexpr INLINE

namespace hash
{

// Identifiers are hashed strings (case sensitive!)
// Templated at class level because hashes might be different

template< typename char_t >
class IdType
{
	struct holder_t
	{
		const char_t* str;
		holder_t( const char_t* str ) : str(str) {}
	};
public:
	typedef hash::hash_t hash_t;
	typedef hash_t::type type_t;

	//------------------------------------------------
	// Constructors

	// Default construct to 'invalid identifier'
	inline IdType() : _hash(0u) {}

	// Construct from string literal, MUST BE FORCEINLINED!
	template< size_t S >
	constexpr IdType( const char_t (&str)[S] ) : _hash(HASH(str)) {}
	// Construct from C-string
	inline IdType( holder_t h ) : _hash(h.str?hash::hash(h.str):0u) {}

	// Construct from hash
	inline IdType( type_t id ) : _hash(id) {}

	//------------------------------------------------
	// Assignment

	// Assign from string literal, MUST BE FORCEINLINED!
	template< size_t S >
	inline IdType& operator= ( const char_t (&str)[S]) { _hash = HASH(str); return *this; }

	// Assign from C-string
	inline IdType& operator= ( holder_t h ) { _hash = (h.str?hash::hash(h.str):0u); return *this; }

	// Assign from hash
	inline IdType& operator= ( type_t id ) { _hash = id; return *this; }

	//------------------------------------------------
	// Comparison

	// Compare to string literal, MUST BE FORCEINLINED!
	template< size_t S >
	inline bool operator== ( const char_t (&str)[S]) const { return _hash==HASH(str); }

	// Compare to C-string
	inline bool operator== ( holder_t h ) const { return _hash==(h.str?hash::hash(h.str):0u); }

	// Compare to hash (let operator below take care of this!)
	//inline bool operator== ( type_t id ) { return _hash==id; }

	//------------------------------------------------
	// Accessors

	// Seamlessly make us act as the hash
	inline operator type_t () const { return _hash; }

private:
	type_t _hash;
};

}

#undef inline
#undef constexpr
